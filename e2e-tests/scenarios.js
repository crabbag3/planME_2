//These are the system tests

'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('planMe', function() {

  describe('signup', function() {

    beforeEach(function() {
      browser.get('index.html#/signup');
    });

    it('should render signup screen when user navigates to /signup', function() {
      expect(element.all(by.id('signup-btn')).getText()).toMatch(/SIGNUP/);

      var firstNameInput = element(by.model('firstName'));
      var randomFirstName = getRandomString(4);
      firstNameInput.sendKeys(randomFirstName);

      var lastNameInput = element(by.model('lastName'));
      var randomLastName = getRandomString(6);
      lastNameInput.sendKeys(randomLastName);

      var emailInput = element(by.model('email'));
      emailInput.sendKeys(randomLastName+"@"+randomLastName+".com");

      var passwordInput = element(by.model('password'));
      passwordInput.sendKeys("abc123");

      var signUpButton = element(by.id('signup-btn'));

      signUpButton.click();
      browser.waitForAngular();

      browser.sleep(5000).then(function(){
       
        expect(browser.getLocationAbsUrl()).toMatch("/");
        var userGreeting = element(by.id('user-greeting'));        
        expect(userGreeting.getText()).toBe('Hi '+randomFirstName);
  
      });
      
    });

  });


  /*describe('event', function() {

    beforeEach(function() {
      browser.get('index.html#!/event');
    });


    it('should render event when user navigates to /event', function() {
      expect(element.all(by.css('[ng-view] h3')).first().getText()).
        toMatch(/Event/);
    });

  });*/
});


var getRandomString = function(length) {
  var string = '';
  var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' //Include numbers if you want
          for (var i = 0; i < length; i++) {
              string += letters.charAt(Math.floor(Math.random() * letters.length));
          }
          return string;
}

var getRandomNum = function(min, max){
  return parseInt(Math.random() * (max - min) + min);
};