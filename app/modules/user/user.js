'use strict';


angular.module('planMe.user', [])


.config(function($stateProvider, $urlRouterProvider, $locationProvider) {    
    
    
    $stateProvider.state('signup', {
        
            url: '/signup',
            templateUrl: 'modules/user/signup.html',
            controller: 'SignupCtrl',
						resolve: {
								
								notAuthenticated: function($q,AuthenticationService) {
										
									 var defer = $q.defer();
										
									 AuthenticationService.$waitForSignIn().then(function(user){
											 
											 if(user)
											 {
											 		defer.reject("ALREADY_AUTHENTICATED");
											 }
											 else
											 {
													defer.resolve("OK_TO_AUTHENTICATE");		 
											 }
											 
									 });
										
										return defer.promise;
							
								}
					}
							
    });
    

    $stateProvider.state('login', {
        
            url: '/login',
            templateUrl: 'modules/user/login.html',
            controller: 'LoginCtrl',
						resolve: {
								
								notAuthenticated: function($q,AuthenticationService) {
										
									 var defer = $q.defer();
										
									 AuthenticationService.$waitForSignIn().then(function(user){
											 
											 if(user)
											 {
											 		defer.reject("ALREADY_AUTHENTICATED");
											 }
											 else
											 {
													defer.resolve("OK_TO_AUTHENTICATE");		 
											 }
											 
									 });
										
										return defer.promise;
							
								}
							
						}
    });
    
    
    $stateProvider.state('account', {
        
            abstract:true,
            url: '',
            templateUrl: 'modules/user/account.html',
            controller: 'AccountCtrl',
						resolve: {
								
								userAccount: function(AuthenticationService) {
									// $requireSignIn returns a promise so the resolve waits for it to complete
									// If the promise is rejected, it will throw a $stateChangeError (see above)
									return AuthenticationService.$requireSignIn();
									//return AuthenticationService.$waitForSignIn();
								}
						}

    });
    
		
    $stateProvider.state('account.dashboard', {
        
            url: '/',
			abstract:true,
			templateUrl: "modules/user/dashboard.html",
            controller: 'DashboardCtrl'

	});
	
	$stateProvider.state('account.dashboard.components', {
        
			url: '',
			views:{
				'view-A':{
					template:"view A",
					controller: function(){}
				},
				'view-B':{
					template:"view B",
					controller: function(){}
				},
				'view-C':{
					template:"view C",
					controller: function(){}
				}
			}

	});
	
	$stateProvider.state('account.profile', {

		url:'/profile/:userId',
		templateUrl: 'modules/user/profile.html',
		controller: function($scope,$stateParams,UserService, profile){

			profile.$bindTo($scope, "userProfile");
			$scope.userProfileEdit =  angular.copy(profile);


			$scope.updateProfile = function() {

				angular.copy($scope.userProfileEdit, $scope.userProfile);
				alert("Profile Updated");
			};

		},
		resolve: {
			
			profile: function($stateParams,UserService) {

				return UserService.getUserProfileForUserId($stateParams.userId).$loaded();
			}
	}

	});

})



//controllers (these handle the UI)    
.controller('SignupCtrl', function($scope,$state, AuthenticationService,UserService) {

    $scope.createUser = function() {
      
			$scope.message = null;
      $scope.error = null;

      // Create a new user
      AuthenticationService.$createUserWithEmailAndPassword($scope.email, $scope.password)
        .then(function(user) {
        
					$scope.message = "User created with uid: " + user.uid;
					
					//after creating the new user lets create their profile and fill it with default data then we can transition them to a logged in state

					UserService.createUserProfile({
						userId:user.uid, 
						firstName: $scope.firstName, 
						lastName:$scope.lastName, 
						photoUrl: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png"})
						
						.then(function(ref) {

							//ref.key() === userProfile.$id; // true
							$state.go('account.dashboard.components');

						}, function(error) {

							$scope.error= error.code + ' ' + error.message;
						
						});
					
        
				}).catch(function(error) {
          
					$scope.error= error.code + ' ' + error.message;
      	
				});
    };
		
		//this method gets called when the authentication state changed we use it to check if we should switch to online mode (ie.go to account dashboard state)
		AuthenticationService.$onAuthStateChanged(function(user) {
				
				if (user) {

					console.log("Signed in as:", user.uid);
					$state.go('account.dashboard.components');
						
				} else {
					
					
					console.log("Signed out");
						
				}
		});
    
})

.controller('LoginCtrl', function($scope, $state, AuthenticationService) {

    $scope.login = function() {
      $scope.message = null;
      $scope.error = null;
          
      AuthenticationService.$signInWithEmailAndPassword($scope.email, $scope.password)
        .then(function(authData) {
          
					$scope.message = "Logged in as:" + authData;
			
        
        }).catch(function(error) {
          
          $scope.error= error.code + ' ' + error.message;
        
      });
   
    };
		
		//this method gets called when the authentication state changed we use it to check if we should switch to online mode (ie.go to account dashboard state)
		AuthenticationService.$onAuthStateChanged(function(user) {
				
				if (user) {

					console.log("Signed in as:", user.uid);
					$state.go('account.dashboard.components');
						
				} else {
					
					
					console.log("Signed out");
						
				}
		});
    
})  
     
.controller('AccountCtrl', function($scope,$state,AuthenticationService, userAccount, UserService) {

		$scope.user = {};
		$scope.user.account = userAccount;
		$scope.user.profile = UserService.getUserProfileForUserId(userAccount.uid);

		
		$scope.logout = function (){
			
				AuthenticationService.$signOut().then(function() {
					// Sign-out successful.
						
				}).catch(function(error) {
					// An error happened.
					alert(error.code + ' ' + error.message);
				});
				
		};
		
		//this method gets called when the authentication state changed we use it to check if we should switch to offline mode (ie.go to login state)
		AuthenticationService.$onAuthStateChanged(function(user) {
				
				if (user) {

					console.log("Signed in as:", user.uid);

				} else {
					
					$state.go('login');
					console.log("Signed out");
						
				}
		});
		
})

.controller('DashboardCtrl', function($scope, EventService) {

	$scope.upcomingEvents = EventService.getEvents();
})


//services
.service('AuthenticationService',function($firebaseAuth) {

    return $firebaseAuth();
         
})

.service('UserService',function(AuthenticationService, $firebaseObject) {

    this.getLoggedInUserId = function(){
        
        return AuthenticationService.$getAuth().uid;
    
	};

	this.createUserProfile = function (newUserProfile){

		var userProfileRef = firebase.database().ref().child("userProfile/"+newUserProfile.userId).set(newUserProfile);

	};

	this.getUserProfileForUserId = function(userId){

		var userProfileRef = firebase.database().ref().child("userProfile").child(userId);
		var userProfile = $firebaseObject(userProfileRef);
		return userProfile;
	};
         
});