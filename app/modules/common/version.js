'use strict';

angular.module('planMe.version', [
  'planMe.version.interpolate-filter',
  'planMe.version.version-directive'
])

.value('version', '1.0.0');
