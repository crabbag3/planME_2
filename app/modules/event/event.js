'use strict';


angular.module('planMe.event', ['planMe.poll','planMe.task'])


.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider.state('account.events', {
        
            url: '/events',
            templateUrl: 'modules/event/events.html',
            controller: 'EventsCtrl'
	});
	

    $stateProvider.state('account.event', {
        
			url: '/event/{eventId}',
			templateUrl: "modules/event/event.html",
			abstract:true,
			controller: "EventCtrl",
			resolve:{
			
				event: function($stateParams,EventService,$q){
								
					var deferred = $q.defer();
								
					EventService.getEvent($stateParams.eventId).$loaded().then(function(event){
			
						if(event.$value!==null){
											
							deferred.resolve(event);
											
						}
						else
						{
							deferred.reject('NOT_FOUND');
						}

					});
								
					return deferred.promise;
								 
				}
							
			}
			
	});

	$stateProvider.state('account.event.details', {
        
            url: '',
			templateUrl: 'modules/event/details.html'
	});

	$stateProvider.state('account.event.members', {
        
            url: '/members',
			templateUrl: 'modules/event/members.html'
	});
	
})


//controllers
.controller('EventsCtrl', function($scope, EventService) {	
	
		//lets load our events
		$scope.events = EventService.getEvents();
	
		//call this to create a blank event (You can go and edit it inline after)
		$scope.createEvent= function (){
			
			var newEvent = {title:'Untitled Event'};
			EventService.createEvent(newEvent);
		
		};

		$scope.saveEvent = function (event){

			EventService.saveEvent(event);

		}

	
		//delete the event passed in as parm
		$scope.deleteEvent = function(event){
			
			 EventService.deleteEvent(event);
		}
	
})

.controller('EventCtrl', function($scope, EventService, event) {
		
		event.$bindTo($scope, "event");
		$scope.eedit =  angular.copy(event);

		$scope.saveEvent = function() {
			
			angular.copy($scope.eedit, $scope.event);
			alert("Event saved");
		};
})



//services
.service('EventService',function($firebaseArray,$firebaseObject, UserService) {

		this.events = [];

		this.createEvent = function (event){
			
			//before adding the event to firebase lets make the current user a member of the event and give the user admin role
			var userId = UserService.getLoggedInUserId();
			var members = {};
			members[UserService.getLoggedInUserId()]={userId:userId, role:"Admin"};
			event.members=members;
			
			//lets get the events and add our new event to the array which will synchronise with firebase
			return this.events.$add(event);
		
		};
		
		this.saveEvent = function(updatedEvent){
			
			this.events.$save(updatedEvent);
			
		}
	
	   this.deleteEvent = function(event){
			
			return this.events.$remove(event);
			
		};
	
		this.getEvents = function (){
			
			var eventsRef = firebase.database().ref().child("events").orderByChild("members/" + UserService.getLoggedInUserId()+"/userId").equalTo(UserService.getLoggedInUserId());
			this.events = $firebaseArray(eventsRef);
			return this.events;
		};

		this.getEvent = function(eventId){
			
			var eventRef = firebase.database().ref().child("events").child(eventId);
			var event = $firebaseObject(eventRef);
			return event;
		}

         
});