'use strict';

// Declare app level module which depends on config, filters, services, etc

angular.module('planMe.poll', [])

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    
	$stateProvider.state('account.event.polls', {
        
            url: '/polls',
			templateUrl:"modules/poll/polls.html",
			controller: function($scope,PollService){
				
				$scope.Polls = PollService.getPollsByEventId($scope.event.$id);

			}
	});
	
	$stateProvider.state('account.event.poll', {
        
            url: '/poll/:pollId',
            templateUrl: 'modules/poll/poll.html',
            controller: 'PollCtrl'
    });
	
    $stateProvider.state('account.event.createPoll', {
        
            url: '/poll/create',
            templateUrl: 'modules/poll/create.html',
            controller: 'CreatePollCtrl'
    });

})


.controller('PollsCtrl', ['$rootScope', '$scope', 'PollService', function($rootScope, $scope, PollService) {
	
	$scope.Polls = PollService.getPolls();
	
}])

.controller('CreatePollCtrl', ['$rootScope', '$scope', '$state', 'PollService', function($rootScope, $scope, $state, PollService) {
	
	$rootScope.section = "New Poll";
	
	// Define an empty poll model object
	$scope.poll = {
		eventId: $scope.event.$id,
		question: '',
		totalVotes: 0,
		choices: [ { text: '', votes: 0 }, { text: '', votes: 0 }, { text: '', votes: 0 }]
	};

	// Method to add an additional choice option
	$scope.addChoice = function() {
		$scope.poll.choices.push({ text: '', votes: 0 });
	};
	
	// Validate and save the new poll to the database
	$scope.createPoll = function() {
	
		// Check that a question was provided
		if($scope.poll.question.length > 0) {
			var choiceCount = 0;
			
			// Loop through the choices, make sure at least two provided
			for (var i = 0, ln = $scope.poll.choices.length; i < ln; i++) {
				var choice = $scope.poll.choices[i];
				
				if (choice.text.length > 0) {
					choiceCount++
				}
			}
		
			if(choiceCount > 1) {
				PollService.addPoll($scope.poll).then(function(poll){
					var pollId = poll.key;
					$state.go('account.event.poll',{"pollId":pollId});
				});
				
			} else {
				alert('You must enter at least two choices');
			}
			
		} else {
			alert('You must enter a question');
		}
	};
		
}])

.controller('PollCtrl', ['$rootScope', '$scope', '$stateParams','PollService', function($rootScope, $scope, $stateParams,PollService) {
	

	$scope.poll = PollService.getPoll($stateParams.pollId);
	
	$scope.poll.$loaded().then(function(p) {
		$scope.poll === p;	
	});
		
	$scope.vote = function() {
		var choice = $scope.userVote;
		
		if(choice) {
			$scope.userVoted = true;
			$scope.poll.totalVotes++;
			$scope.poll.choices[choice].votes++;
			$scope.poll.$save();
		} else {
			alert('You must select an option to vote for');
		}
	};
}])

//services
.service('PollService',function($firebaseArray,$firebaseObject) {
    
	  
		this.addPoll = function (poll){
             var pollsRef = firebase.database().ref().child("polls");
	         var polls = $firebaseArray(pollsRef);
			 return polls.$add(poll);
		
		};
    
	
	  this.getPoll = function(pollId){
			
            var pollsRef = firebase.database().ref().child("polls");
	        var polls = $firebaseArray(pollsRef);
			var pollRef = pollsRef.child(pollId);
			var poll = $firebaseObject(pollRef);
			return poll;
		}
		
		this.savePoll = function(poll){
            
			return poll.$save();
			
		}
	
	  this.deletePoll = function(poll){
			
			 return polls.$remove(poll);
			
		};
	
		this.getPolls = function (){
            var pollsRef = firebase.database().ref().child("polls");
	        var polls = $firebaseArray(pollsRef);
			return polls;
		};

		this.getPollsByEventId = function(eventId){

			var pollsRef = firebase.database().ref().child("polls").orderByChild("eventId").equalTo(eventId);
			var polls = $firebaseArray(pollsRef);
			return polls;
		};

         
});
