// This is the entry point file for angular js

'use strict';

//create main application module and inject other modules which we need as dependencies
var planMe = angular.module('planMe', [
    'firebase',
    'ui.router',
    'xeditable',
    'planMe.user',
    'planMe.event',
    'planMe.version'
]);

//configure the application
planMe.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

		//Initialize Firebase connection
    firebase.initializeApp({
    	apiKey: "AIzaSyAc5l-zI4M-ZOVZgSayPqhiiuthxlC7r5c",
    authDomain: "planme-ff67e.firebaseapp.com",
    databaseURL: "https://planme-ff67e.firebaseio.com",
    projectId: "planme-ff67e",
    storageBucket: "planme-ff67e.appspot.com",
    messagingSenderId: "188436086061"
    });
		
		//lets create some general application states (404 Not found will be transitioned to whenever a bad url was entered)
    $urlRouterProvider.otherwise('/404');

    $stateProvider.state('404', {

        url: '/404',
        templateUrl: '404.html'
    });
});


// This 
planMe.run(function($rootScope,$transitions,$state,AuthenticationService,editableOptions){
	
		editableOptions.theme = 'bs3';
	
	
    //location event handlers
    $rootScope.$on('$locationChangeStart',function(event, next, current){

        console.log('Location Change:' + ' from '+ current + ' to '+ next);
    });

    $rootScope.$on('$locationChangeSuccess',function(event, next, current){

        console.log('Location Change Success:' + ' from '+ current + ' to '+ next);
    });



    //state transition event handlers
    $transitions.onStart({}, function($transition){
        console.log('State transition start: transition begins:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());				
    });
		
    $transitions.onError({}, function($transition){
        console.log('State transition start: transition begins:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());
				
			 //if we tried to transition to a state that requires authentication but we were not logged in lets go to the login state	
		   if ($transition._error.detail === "AUTH_REQUIRED") {
      		
					 $state.go("login");
    	 }
				
			 //if we are already logged in and we try to log in again or signup lets redirect to the dashboard of the logged in user
			 else if($transition._error.detail === "ALREADY_AUTHENTICATED"){
					
					 $state.go("account.dashboard.components");
				
			 }
				//if for some reason we throw a NOT_FOUND error lets go to the 404 Error screen
				else if($transition._error.detail === "NOT_FOUND")
				{
						$state.go("404");
				}
				
    });

    $transitions.onExit({}, function($transition){
        console.log('State transition exit:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());
    });

    $transitions.onRetain({}, function($transition){
        console.log('State transition retain:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());
    });

    $transitions.onEnter({}, function($transition){
        console.log('State transition enter:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());	
				
    });

    $transitions.onFinish({}, function($transition){
        console.log('State transition finish:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());
    });

    $transitions.onSuccess({}, function($transition){
        console.log('State transition success:' + ' from '+ $transition.$from() + ' to '+ $transition.$to());
    });


    //view event handlers
    $rootScope.$on('$viewContentLoading',function(event, viewConfig){

        console.log('View Load: the view is loading, and DOM rendering!');
    });

    $rootScope.$on('$viewContentLoaded',function(event, viewConfig){

        console.log('View Load: the view is loaded, and DOM rendered!');

    });

});
